#include <omp.h>
#include <stdio.h>

int main () {

    printf("\nOla 1 - Fora da Regiao Paralela....\n\n");

    #pragma omp parallel num_threads(4)
    {
        int id = omp_get_thread_num();
        int nt = omp_get_num_threads();

        printf("Sou a thread %d de um total de %d \n", id, nt);
    }

    return 0;
}
