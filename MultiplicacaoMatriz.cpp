#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main() {
	int dimensao = 256;	
	int i, k, j;
	time_t start,end;		
	float **A;
	float **B;
	float **C;
	
	// aloca os limites da matriz
	A = (float **) calloc(dimensao, sizeof(float *));
	B = (float **) calloc(dimensao, sizeof(float *));
	C = (float **) calloc(dimensao, sizeof(float *));
	
	// aloca as colunas da matriz
	for ( i = 0; i < dimensao; i++ ) {
		A[i] = (float *) calloc (dimensao, sizeof(float));
		B[i] = (float *) calloc (dimensao, sizeof(float));
		C[i] = (float *) calloc (dimensao, sizeof(float));
	}	
	//inicializar matriz A
	for (i=0;i<dimensao;i++){
		for (j=0;j<dimensao;j++){
			A[i][j]=(2*i)+j;
		}
	}	
	//inicializar matriz B
	for (i=0;i<dimensao;i++){
		for (j=0;j<dimensao;j++){
			B[i][j]=(3*i)-j;
		}
	}	
	//inicializar matriz C 	
	for (i=0;i<dimensao;i++){
		for (j=0;j<dimensao;j++){
			C[i][j]=0;
		}
	}	
	
	
	//multiplicacao de A por b
	start = clock();	
	for (i=0;i<dimensao;i++) {
		for (k=0;k<dimensao;k++) {
			for (j=0;j<dimensao;j++) {
				C[i][j]=A[i][k]*B[k][j];
			}
		}
	}	
	end = clock() - start;	
	printf("\nTempo de exe comum.: %lf s\n", ((double)end) / CLOCKS_PER_SEC);
	
	
	//multiplicacao de A por b
	start = clock();	
	for (i=0;i<dimensao;i++) {
		for (k=0;k<dimensao;k++) {
			for (j=0;j<dimensao;j=j+2) {
				C[i][j]=A[i][k]*B[k][j];
				C[i][j+1]=A[i][k]*B[k][j+1];
			}
		}
	}	
	end = clock() - start;	
	printf("\nTempo de exe two unrolling.: %lf s\n", ((double)end) / CLOCKS_PER_SEC);	
	
	
	//multiplicacao de A por b
	start = clock();	
	for (i=0;i<dimensao;i++) {
		for (k=0;k<dimensao;k++) {
			for (j=0;j<dimensao;j=j+4) {
				C[i][j]=A[i][k]*B[k][j];
				C[i][j+1]=A[i][k]*B[k][j+1];
				C[i][j+2]=A[i][k]*B[k][j+2];
				C[i][j+3]=A[i][k]*B[k][j+3];
			}
		}
	}	
	end = clock() - start;		
	printf("\nTempo de exe four unrolling.: %lf s\n", ((double)end)/CLOCKS_PER_SEC);
	
			
	system("pause");	
	//return 0;
}
